module.exports = {
  root: true,
  env: {
    node: true,
  },

  extends: [
    "plugin:vue/recommended",
    "@vue/airbnb",
    "@vue/typescript/recommended",
    'plugin:prettier-vue/recommended',
    'prettier',
  ],

  plugins: ["unused-imports"],
  parser: "vue-eslint-parser",

  parserOptions: {
    parser: "@typescript-eslint/parser",
    ecmaVersion: 2018,
    sourceType: "module",
    project: "./tsconfig.json"
  },

  rules: {
    'prettier-vue/prettier': [
      'error',
      {
        // Override all options of `prettier` here
        // @see https://prettier.io/docs/en/options.html
        printWidth: 120,
        singleQuote: false,
        semi: true,
        trailingComma: 'es5',
      },
    ],
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "camelcase": [0],
    "@typescript-eslint/camelcase": "off",
    "class-methods-use-this": "off",
    "no-unused-vars": "off",
    "no-useless-constructor": "off",
    "quotes": ["error", "double"],
    "@typescript-eslint/no-useless-constructor": "error",
    "unused-imports/no-unused-imports": "error",
    "unused-imports/no-unused-vars": [
      "warn",
      { vars: "all", varsIgnorePattern: "^_", args: "after-used", argsIgnorePattern: "^_" },
    ],
    "max-len": [1, 120, 2],
    "@typescript-eslint/naming-convention": [
      "error",
      {
        "selector": "variable",
        "types": ["boolean"],
        "format": ["PascalCase"],
        "prefix": ["is", "should", "has", "can", "did", "will"]
      }
    ],
    "lines-between-class-members": ["error", "always", { "exceptAfterSingleLine": true }],
    'no-underscore-dangle': 'off',

  },

  overrides: [
    {
      files: ["**/__tests__/*.{j,t}s?(x)", "**/tests/unit/**/*.spec.{j,t}s?(x)"],
      env: {
        jest: true,
      },
    },
  ],
};
