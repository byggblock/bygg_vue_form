export interface VeeValidate {
  validate(): Promise<boolean>;
  reset(): void;
  resetValidation(): void;
}
