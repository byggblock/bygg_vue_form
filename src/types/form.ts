export type ActionOptions = "update" | "create";

export type FieldTypes =
  | "text"
  | "select"
  | "autocomplete"
  | "checkbox"
  | "image"
  | "colorPicker"
  | "divider"
  | "column";

export interface FormModel {
  [key: string]: unknown;
}

interface BaseFieldSettings {
  /** Pass model ID that will be used in requests. If undefined uses $route.params.id */
  id?: number | string;
  /** Should be the same as the key name in the response */
  name?: string;
  /** Title of field */
  label?: string;
  /** Initial value of field, default undefined */
  value?: unknown;
  /** VeeValidate rules, see vee-validate-rules.ts (can have multiple rules, ex: rules: "required | email") */
  rules?: string;
  /**
   * @enum image: Select/preview image with upload
   * @enum combobox: Autocomplete that allows user to enter values that do not exist
   * @enum divider: Simple horizontal line to divide sections, can include name
   * @enum column: empty column for design purpose (can also use fieldWidth to prevent large fields)
   * TODO: add combobox https://vuetifyjs.com/en/components/combobox/
   */
  type?: FieldTypes;
  /** Multiple selections when using type "select" */
  multiple?: boolean;
  /** Items passed to field, used by select */
  items?: unknown[];
  /** Calls method and add response data to items (used for select) */
  fetchItems?(): Promise<Record<string, unknown>[]>;
  itemText?: string;
  itemValue?: string;
  /** Hide on create and/or edit */
  hideOn?: ActionOptions[];
  class?: string;
  group?: string;
  showOnParentValues?: [boolean | string | number];
  fieldWidth?: string;
}

export interface ChildFieldSettings extends BaseFieldSettings {
  showOnParentValues?: [boolean | string | number];
  disableOnParentValues?: [boolean | string | number];
}

export interface FieldSettings extends BaseFieldSettings {
  children?: ChildFieldSettings[];
}

export interface Actions {
  fetch(id: string | number): Promise<Record<string, never>>;
  update(id: string | number, data: FormModel): Promise<void>;
  create(data: FormModel): Promise<void>;
}

export interface Serializers {
  create(data: FormModel): Record<string, never>;
  update(data: FormModel): Record<string, never>;
  data(data: FormModel): Record<string, never>;
}

export interface FormSettings {
  modelName?: string;
  /**
   * Will force use selected type action (post, update).
   * By default it will check route for params.id (update if route param found)
   */
  type?: "create" | "update";
  /**
   *  Shows error notification with given string
   */
  error?: string;
  /**
   *  Promise methods for fetch/update/create
   */
  actions: Actions;
  /**
   *  Serializers for create/update/data
   *  Data serializer can be used to format the fetch results (for ex. nested attributes in response  )
   */
  serializers: Serializers;
  /**
   *  Redirect to given route name after submit
   */
  redirectRoute: Serializers;
  /**
   *  Hides submit button
   */
  manualSubmit: boolean;
  /**
   *  Disable fetching the model (will not execute actions.fetch)
   */
  manualModelFetch: boolean;
}
