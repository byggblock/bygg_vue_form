export default interface Notification {
  type: "error" | "warning" | "success" | "info";
  text: string;
  // Defaults to "alerts"
  group?: string;
  // Duration in milliseconds, use -1 to keep open indefinitely
  duration?: number;
}
