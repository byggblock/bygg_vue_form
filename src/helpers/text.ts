// eslint-disable-next-line import/prefer-default-export
export function capitalizeFirstLetter(string: string | undefined): string {
  return string ? string.charAt(0).toUpperCase() + string.slice(1) : "";
}
