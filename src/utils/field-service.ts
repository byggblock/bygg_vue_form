import { capitalizeFirstLetter } from "@/helpers/text";
import { ActionOptions, ChildFieldSettings, FieldSettings } from "@/types/form";

export default class FieldService {
  isVisibleByFormActionType(action: ActionOptions, field: FieldSettings): boolean {
    return !field.hideOn?.includes(action);
  }

  isShowOnParentValueEqual(parentValue: unknown, child: ChildFieldSettings): boolean {
    return child.showOnParentValues?.some((value) => value === parentValue) ?? true;
  }

  isDisableFieldParentValueEqual(parentValue: unknown, child: ChildFieldSettings): boolean {
    return child.disableOnParentValues?.some((value) => value === parentValue) ?? false;
  }

  getFieldLabel(field: FieldSettings | null | undefined): string {
    if (!field) return "";
    return field?.label ? field.label : capitalizeFirstLetter(field.name);
  }
}
