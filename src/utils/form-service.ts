import { FieldSettings, FormModel, Serializers } from "@/types/form";
import { Vue } from "vue-property-decorator";
import Repository from "./repository";

export default class FormService {
  fields: FieldSettings[];

  constructor(fields?: FieldSettings[]) {
    this.fields = fields ?? [];
  }

  getFieldByName(name: string | undefined): FieldSettings | undefined {
    if (!name) return undefined;
    return this.fields.find((field) => field.name === name);
  }

  setField(field: FieldSettings): void {
    if (this.isFieldInList(field)) {
      const fieldIndex = this.fields.findIndex((oldField) => oldField.name === field.name);
      this.fields.splice(fieldIndex, 1, field);
      return;
    }
    this.fields.push(field);
  }

  setFields(fields: FieldSettings[]): void {
    this.fields = fields;
  }

  isFieldInList(newField: FieldSettings): boolean {
    return this.fields.some((field) => field.name === newField.name);
  }

  getSerializedModel(): FormModel {
    const formModel: FormModel = {};
    this.fields.forEach((field) => {
      if (!field.name) return;
      formModel[field.name] = field.value;
    });
    return formModel;
  }

  async fetchModel(id: string | number, repository: Repository): Promise<void> {
    return repository
      .fetch(id)
      .then((res) => {
        this.setFieldValuesByFetchedModel(res, repository.serializers);
      })
      .catch((err) => {
        const message = err?.response?.data?.message ?? null;
        return message;
      });
  }

  setFieldValuesByFetchedModel(item: Record<string, unknown>, serializers: Serializers): void {
    let data = item;
    if (serializers.data) {
      data = serializers.data(item);
    }
    Object.entries(data).forEach(([key, value]) => {
      if (this.isFieldInList({ name: key })) {
        const fieldIndex = this.fields.findIndex((e) => e.name === key);
        if (fieldIndex === -1) return;
        Vue.set(this.fields, fieldIndex, { ...this.fields[fieldIndex], value });
      }
    });
  }
}
