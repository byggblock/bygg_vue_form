import { Actions, FormModel, Serializers } from "@/types/form";

export default class Repository {
  serializers = null as unknown as Serializers;
  actions = null as unknown as Actions;

  setSerializers(serializers: Serializers): void {
    this.serializers = serializers;
  }

  setActions(actions: Actions): void {
    this.actions = actions;
  }

  update(id: string | number, model: FormModel): Promise<void> {
    const serializedData = this.serializers?.update(model);

    if (!this.actions?.update) throw new Error("Update promise method missing from actions");
    if (!serializedData) throw new Error("Serialized data is undefined");

    return this.actions.update(id, serializedData);
  }

  create(model: FormModel): Promise<void> {
    if (!this.actions.create) throw new Error("Create method missing from repository");
    const serializedData = this.serializers.create(model);
    return this.actions.create(serializedData);
  }

  async fetch(id: string | number): Promise<Record<string, unknown>> {
    if (!this.actions.fetch) throw new Error("Fetch method missing from actions prop");
    return this.actions.fetch(id);
  }
}
