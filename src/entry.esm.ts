import _Vue, { PluginFunction } from "vue";
import * as components from "@/lib-components/index";

const install: PluginFunction<any> = function installByggVueForm(Vue: typeof _Vue) {
  Object.entries(components).forEach(([componentName, component]) => {
    Vue.component(componentName, component);
  });
};

// Create module definition for Vue.use()
export default install;

// To allow individual component use, export components
// each can be registered via Vue.component()
export * from "@/lib-components/index";

export * from "@/types/form";
