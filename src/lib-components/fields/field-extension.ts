import FieldService from "@/utils/field-service";
import FormService from "@/utils/form-service";
import { FieldSettings, FieldTypes } from "@/types/form";
import { Component, InjectReactive, Prop, Vue } from "vue-property-decorator";

@Component
export default class FieldExtension extends Vue {
  @Prop({ required: true, type: Object as () => FieldSettings }) settings!: FieldSettings;
  @Prop({ default: false }) readonly errors!: string[];
  @Prop({ default: false }) readonly disabled!: boolean;
  @InjectReactive() readonly formService!: FormService;

  isLoading = false;
  fieldService = new FieldService();
  fetchedFieldItems: null | unknown[] = null;
  ignoredFieldTypes: FieldTypes[] = ["column", "divider"];

  get field(): FieldSettings {
    const formServiceField = this.formService.getFieldByName(this.settings?.name ?? "") ?? {};
    return { ...this.settings, ...formServiceField };
  }

  get fieldLabel(): string {
    return this.fieldService.getFieldLabel(this.field);
  }

  setFieldValue(value: unknown): void {
    this.formService.setField({ ...this.field, value });
  }

  get fieldValue(): unknown {
    return this.field?.value ?? undefined;
  }

  get mergedFieldItems(): unknown[] {
    const fieldItems = this.field.items ?? [];
    const fetchedFieldItems = this.fetchedFieldItems ?? [];
    return [...fieldItems, ...fetchedFieldItems];
  }

  fetchFieldItems(): void {
    if (typeof this.field.fetchItems !== "function") return;
    this.isLoading = true;
    this.field.fetchItems().then((items) => {
      this.fetchedFieldItems = items;
      this.isLoading = false;
    });
  }

  mounted(): void {
    if (!this.field.type) return;
    if (!this.ignoredFieldTypes.includes(this.field.type)) {
      this.formService.setField(this.settings);
    }
    this.fetchFieldItems();
  }
}
